package com.accenture.learncamel;

import java.util.Arrays;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.hibernate.validator.internal.util.privilegedactions.GetInstancesFromServiceLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaCamelApplication extends RouteBuilder{

	public static void main(String[] args) {
		SpringApplication.run(FaCamelApplication.class, args);
	}

	@Override
	public void configure() throws Exception {

		System.out.println("Starting...");
//		moveAllFiles();
//		moveSpecificFile("myFile");
//		moveSpecificFileWithBody("Java");
//		fileProcess();
		multiFileProcessor();
		System.out.println("End.");
	}
	
	public void moveAllFiles() {
		// Es key sensitive, y 'file:' no puede estar separado al path.
				// Mientras la app siga corriendo, va a seguir moviendo los archivos en el momento que los paso.
				// El parametro noop en true, evita que se borren, ni se metan en la carpeta .camel los files del fromPath.
		from("file:src/main/resources/folderA?noop=true")
			.to("file:src/main/resources/folderB");
	}
	
	public void moveSpecificFile(String type) {
		from("file:src/main/resources/folderA?noop=true")
			.filter(header(Exchange.FILE_NAME).startsWith(type))
		.to("file:src/main/resources/folderB");			
	}
	
	public void moveSpecificFileWithBody(String content) {
		from("file:src/main/resources/folderA?noop=true")
			.filter(body().startsWith(content))
		.to("file:src/main/resources/folderB");			
	}
	
	public void fileProcess() {
		from("file:src/main/resources/folderA/process?noop=true")
			.process(p->{
				String body = p.getIn().getBody(String.class);
				StringBuilder sb = new StringBuilder();
				Arrays.stream(body.split(" ")).forEach(s->{
					sb.append(s+",");
				});
				
				p.getIn().setBody(sb);
			})
		.to("file:src/main/resources/folderB?fileName=records.csv");			
	}

	// (1) Splitea todo el contenido, separandolo por el token, ',' lo toma como el espacio por lo visto,
	// (2) Distribuye en diferentes .csv (excels) el split que tiene la condicion buscada.
	public void multiFileProcessor() {
		from("file:src/main/resources/folderA/process?noop=true")
			.unmarshal().csv().split(body().tokenize(",")) // (1)
				.choice() // Comienzo de (2)
					.when(body().contains("Closed"))
						.to("file:src/main/resources/folderB?fileName=Close.csv")
					.when(body().contains("Pending"))
						.to("file:src/main/resources/folderB?fileName=Pending.csv")
					.when(body().contains("Interest"))
						.to("file:src/main/resources/folderB?fileName=Interest.csv");
	}

}
